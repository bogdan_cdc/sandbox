function swipedetect(el, callback){

    var touchsurface = el,
        swipedir = 'none',
        startX,
        startY,
        distX,
        distY,
        threshold = 50, //required min distance traveled to be considered swipe
        restraint = 50, // maximum distance allowed at the same time in perpendicular direction
        allowedTime = 300, // maximum time allowed to travel that distance
        elapsedTime,
        startTime,
        handleswipe = callback;

    touchsurface.addEventListener('touchstart', function(e){
        var touchobj = e.changedTouches[0];
        swipedir = 'none';
        var dist = 0;
        startX = touchobj.pageX;
        startY = touchobj.pageY;
        startTime = new Date().getTime();
        // record time when finger first makes contact with surface
    }, false)

    touchsurface.addEventListener('touchend', function(e){
        var touchobj = e.changedTouches[0]
        distX = touchobj.pageX - startX; // get horizontal dist traveled by finger while in contact with surface
        distY = touchobj.pageY - startY; // get vertical dist traveled by finger while in contact with surface
        elapsedTime = parseInt(new Date().getTime()) - parseInt(startTime) // get time elapsed
        if (elapsedTime <= allowedTime){ // first condition for awipe met
            if (Math.abs(distX) >= threshold && Math.abs(distY) <= restraint){ // 2nd condition for horizontal swipe met
                swipedir = (distX < 0)? 'left' : 'right' // if dist traveled is negative, it indicates left swipe
                // e.preventDefault()
            }
            else if (Math.abs(distY) >= threshold && Math.abs(distX) <= restraint){ // 2nd condition for vertical swipe met
                swipedir = (distY < 0)? 'up' : 'down' // if dist traveled is negative, it indicates up swipe
            }
        }
        
        handleswipe(swipedir)
    }, false)

    touchsurface.addEventListener('mousedown', function(e){
        swipedir = 'none';
        startX = e.pageX;
        startY = e.pageY;
        // e.target.parentNode.style.opacity = '.5'
    })
    
    touchsurface.addEventListener('mouseup', function(e){
        
        distX = e.pageX - startX;
        distY = e.pageY - startY;
        
        if (Math.abs(distX) >= threshold && Math.abs(distY) <= 100){ // 2nd condition for horizontal swipe met
            swipedir = (distX < 0)? 'left' : 'right' // if dist traveled is negative, it indicates left swipe
        }
        else if (Math.abs(distY) >= threshold && Math.abs(distX) <= 100){ // 2nd condition for vertical swipe met
            swipedir = (distY < 0)? 'up' : 'down' // if dist traveled is negative, it indicates up swipe
        }
        handleswipe(swipedir)
    })
}

$('.uvSlider').each(function(index, el) {
    for (var i = 0; i < $(el).find('.uvSliderItem').length; i++) {
        $(el).find('.uvSlider__pagination').append('<li class="uvSlider__pagination__item navSonar"><button class="sr-only" aria-controls="carousel">Toggle slide ' + (i + 1) + '</button></li>');
    }
    $(el).find('.uvSlider__pagination__item').eq($('.uvSliderItem.active').index()).addClass('active');
});

$('.uvSliderItem.active').each(function(index, el) {
    $(el).velocity({
        opacity: 1,
        complete: function () {
            $(el).attr('aria-hidden', 'false')
        }
    })
});

function uvSlideHide(act, dir, effect) {
    var d = 1;
    if (dir == 'prev') {
        d = -1;
    }

    if (effect == undefined || effect == 'slide') {
        act.find('.uvSliderItem__mainText').velocity({
            translateX: -294 * d,
        }, {
            duration: 800
        });
        act.find('.uvSliderItem__mainImage').velocity({
            translateX: -294 * d,
        }, {
            duration: 700
        });
    }


    act.velocity({
        opacity: [0, 1],
        complete: function () {
            act.attr('aria-hidden', 'true')
        }
    }, {
        duration: 800
    });
}

function uvSlideShowNext(act, nextAct, slideImage, slideText, dir, effect) {

    var d = 1;
    if (dir == 'prev') {
        d = -1;
    }

    nextAct.addClass('active');
    
    slideText.velocity({
        opacity: 0
    }, {
        duration: 0
    });

    nextAct.velocity({
        // translateX: [0, 294 * d],
        opacity: [1, 0],
        complete: function () {
            nextAct.attr('aria-hidden', 'false')

            if ($('._height_recalc').length) {
                nextAct.parents('.uvSliderItemsList').outerHeight(nextAct.find('._height_recalc').outerHeight())
            }
        }
    }, {
        duration: 800
    });

    nextAct.find('.ghost_text').velocity({
        translateX: [0, 294 * d],
        opacity: [1, 0],
    }, {
        duration: 1200,
        easing: [ 0.24, 0.57, 0.47, 0.8]
    });

    if (effect == undefined) {
        nextAct.find('.uvSliderItem__background').velocity({
            scale: [1, 1.1]
        }, {
            duration: 2000
        });

        slideImage.velocity({
            translateX: [0, -200 * d],
            opacity: [1, 0]
        }, {
            duration: 800
        });

        slideText.velocity({
            translateX: [0, -124 * d],
            opacity: [1, 0]
        }, {
            duration: 800
        });
    } else if (effect == 'slide') {
        nextAct.find('.uvSliderItem__background').velocity({
            translateX: [0, -100 * d]
        }, {
            duration: 800
        });

        slideImage.velocity({
            translateX: [0, -200 * d],
            opacity: [1, 0]
        }, {
            duration: 800
        });
        
        slideText.velocity({
            translateX: [0, -124 * d],
            opacity: [1, 0]
        }, {
            duration: 800
        });
    } else if (effect == 'fade') {
        nextAct.find('.uvSliderItem__background').velocity({
            opacity: [1, 0]
        }, {
            duration: 800
        });

        slideImage.velocity({
            opacity: [1, 0]
        }, {
            duration: 800
        });
        
        slideText.velocity({
            opacity: [1, 0]
        }, {
            duration: 800
        });
    }

}

function initUvSliderMove(dir, swipedir, toIndex, activeItem, activeItemIndex, uvItemsLength, parent, effect) {

    if (uvItemsLength > 1) {
    

        activeItem.removeClass('active');

        $('.velocity-animating').each(function() {
            $.Velocity.mock = true;
            $(this).velocity("finish");
            $.Velocity.mock = false;
        });

        uvSlideHide(activeItem, dir, effect);

        var d = 1;

        if (dir == 'prev') {
            d = -1;
        } else if (dir == 'stay') {
            d = 0;
        }

        var nextSlideIndex = activeItemIndex + d;
        if (toIndex != undefined) {
            nextSlideIndex = toIndex;
        } else if (nextSlideIndex < 0) {
            nextSlideIndex = uvItemsLength - 1;
        } else if (nextSlideIndex > uvItemsLength - 1) {
            nextSlideIndex = 0;
        }
        
        parent.find('.uvSlider__pagination__item').eq(nextSlideIndex).addClass('active').siblings().removeClass('active');

        var slide = parent.find('.uvSliderItem').eq(nextSlideIndex),
            slideImage = slide.find('.uvSliderItem__mainImage'),
            slideText = slide.find('.uvSliderItem__mainText');

        if (swipedir != undefined && swipedir == 'left' || swipedir == 'right') {
            uvSlideShowNext(activeItem, slide, slideImage, slideText, dir, effect);
        } else {
            uvSlideShowNext(activeItem, slide, slideImage, slideText, dir, effect);
        }
        
    }
}

$('.uvSlider__nav').on('click', function() {
    var dir = $(this).data('direction'),
        parent = $(this).parents('.uvSlider'),
        effect = parent.data('effect'),
        swipedir = undefined,
        toIndex = undefined,
        activeItem = parent.find('.uvSliderItem.active'),
        activeItemIndex = activeItem.index(),
        uvItemsLength = parent.find('.uvSliderItem').length;

    initUvSliderMove(dir, swipedir, toIndex, activeItem, activeItemIndex, uvItemsLength, parent, effect);
});

$('.uvSlider__pagination__item').on('click', function() {
    var dir = 'next',
        parent = $(this).parents('.uvSlider'),
        effect = parent.data('effect'),
        swipedir = undefined,
        toIndex = $(this).index(),
        activeItem = parent.find('.uvSliderItem.active'),
        activeItemIndex = activeItem.index(),
        uvItemsLength = parent.find('.uvSliderItem').length;

    if ($(this).index() < parent.find('.uvSlider__pagination__item.active').index()) {
        dir = 'prev';
    }
    if (!$(this).hasClass('active')) {
        initUvSliderMove(dir, swipedir, toIndex, activeItem, activeItemIndex, uvItemsLength, parent, effect);
    }
});

var swiperElements = document.getElementsByClassName('uvSliderItemsList');

for (let index = 0; index < swiperElements.length; index++) {
    const element = swiperElements[index];
    
    swipedetect(element, function(swipedir){
        var dir = 'prev',
            parent = $(element).parents('.uvSlider'),
            effect = parent.data('effect'),
            swipedir = swipedir,
            toIndex = undefined,
            activeItem = parent.find('.uvSliderItem.active'),
            activeItemIndex = activeItem.index(),
            uvItemsLength = parent.find('.uvSliderItem').length;

        if (swipedir == 'left') {
            dir = 'next'
        }
        if (swipedir != 'none') {
            initUvSliderMove(dir, swipedir, toIndex, activeItem, activeItemIndex, uvItemsLength, parent, effect);
        }
    });
    
}