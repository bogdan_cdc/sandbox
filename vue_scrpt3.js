document.addEventListener('DOMContentLoaded', () => {
  var date =  new Date();
  console.log('loaded ', date)
})

let k = true;

new Vue({
  el: '#app',
  data: {
      name: 'Greetings!',
      subject: '',
      termsAndConditions: '',
      yesOrNo: '',
  },
  methods: {
    submit() {
        console.log('name: ', this.name);
        console.log('subject: ', this.subject);
        console.log('termsAndConditions: ', this.termsAndConditions);
        console.log('yesOrNo: ', this.yesOrNo);
    }
  },
})


new Vue({
  el: '#app2',
  data: {
    km: 0
  },
  computed: {
    m: {
      get() {
        // console.log(this)
        return this.km * 1000
      },
      set(newVal) {
        this.km = newVal / 1000
      }
    }
  }
});

function loadF (data1) {
  axios.get(
    'https://jsonplaceholder.typicode.com/users'
  ).then((response) => {
    const data = response.data;
    const randomUser = response.data[
      Math.floor( Math.random() * data.length )
    ];

    data1.name = randomUser.name;
    data1.email = randomUser.email;
    data1.company.name = randomUser.company.name;
    data1.company.catchPhrase = randomUser.company.catchPhrase;
  });
}

new Vue({
  el: '#app3',
  data: {
    name: 'Loading...',
    email: 'Loading...',
    company: {
      name: 'Loading...',
      catchPhrase: 'Loading...',
    }
  },
  methods: {
    upd () {
      loadF (this);
    }
  },
  created() {
    loadF (this);
  },
  mounted() {
    // console.log(this.$el);
  },
  updated() {
    // console.log(this.$el)
  },
});

const tweets = [
  {
    id: 1,
    name: 'James',
    handle: '@james',
    img: 'http://placekitten.com/64/64',
    tweet: "If you don't succeed, dust yourself off...",
    likes: 10,
  },
  {
    id: 2,
    name: 'Fatima',
    handle: '@fatima',
    img: 'http://placekitten.com/g/64/64',
    tweet: "Better late than never but never late...",
    likes: 12,
  },
  {
    id: 3,
    name: 'Xin',
    handle: '@xin',
    img: 'http://placekitten.com/64/64',
    tweet: "Beauty in the struggle...",
    likes: 187,
  },
];

Vue.component('tweet-content', {
  template: `
    <div class="media-content">
      <div class="content">
        <p>
          <strong>{{tweet.name}}</strong>
          <small>{{tweet.handle}}</small>
          <br>
          {{tweet.tweet}}
        </p>
      </div>
      <div class="level-left">
        <a class="level-item">
          <span class="icon is-small">
            <i class="fas fa-heart"></i>
          </span>
          <span class="likes">{{tweet.likes}}</span>
        </a>
        <a class="level-item"
          @click="$emit('add', tweet)">
          <span class="icon is-small">
            <i class="fas fa-plus"></i>
          </span>
        </a>
      </div>
    </div>`,
  props: ['tweet']
});

Vue.component('tweet-component', {
  template: `
  <div class="tweet" style="margin-bottom: 25px;">
    <div class="box" style="margin-bottom: 10px;">
        <article class="media">
            <div class="media-left">
                <figure class="image is-64x64">
                    <img :src="tweet.img" alt="">
                </figure>
            </div>
            <tweet-content :tweet="tweet"
              @add="$emit('add', $event)"
            ></tweet-content>
        </article>
    </div>
    <div class="control has-icons-left">
        <input class="input is-small" placeholder="Tweet your reply..." />
        <span class="icon is-small is-left">
            <i class="fas fa-envelope"></i>
        </span>
    </div>
  </div>`,
  props: {
    tweet: {
      type: Object,
      required: true
    }
  }
})

new Vue({
  el: '#app4',
  data: {
    tweets
  },
  methods: {
    addTweetMessage(tweet) {
      let newTweet = {};
      let lastTweetObjectID = this.tweets[this.tweets.length - 1].id;

      newTweet = Object.assign({}, tweet);
      newTweet.id = lastTweetObjectID + 1;

      this.tweets.push(newTweet);
    }
  }
});



const localComponent = {
  template: `<p class="local"><b>Text in local template:</b> {{ message }}</p>`,
  props: ['message']
}

Vue.component('global-component', {
  template: `<div class="globalWrap"><p class="global"><b>Text in global template:</b> {{ message }}</p> <local-component :message="message2"></local-component></div>`,
  props: ['message', 'message2'],
  components: {
    'local-component': localComponent
  }
})

new Vue({
  el: '#app5',
  data: {
    messageLocal: 'some local text',
    messageGlobal: 'some global text'
  },
  template: `
    <div>
      <local-component :message="messageLocal"></local-component>
      <global-component :message="messageGlobal" :message2="messageLocal"></global-component>
    </div>
  `,
  components: {
    'local-component': localComponent
  }
})

// new Vue({
//   el: '#app5',
//   data: {
//     message: 'Hello World'
//   },
//   components: {
//     'local-component': localComponent
//   }
// })